package com.dummy.myerp.business.impl.manager;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.TransactionStatus;

import com.dummy.myerp.business.impl.manager.ComptabiliteManagerImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.testbusiness.business.BusinessTestCase;


public class ComptabiliteManagerImplTest extends BusinessTestCase{

	private ComptabiliteManagerImpl manager = new ComptabiliteManagerImpl();
	TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();

	@Test
	public void checkEcritureComptable() throws Exception {
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
		vEcritureComptable.setDate(new Date());
		vEcritureComptable.setLibelle("Libelle");
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(1), null, new BigDecimal(123), null));
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(2), null, null, new BigDecimal(123)));
		manager.checkEcritureComptable(vEcritureComptable);
	}

	@Test(expected = FunctionalException.class)
	public void checkEcritureComptableViolation() throws Exception {
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		manager.checkEcritureComptable(vEcritureComptable);
	}

	@Test(expected = FunctionalException.class)
	public void checkEcritureComptableRG2() throws Exception {
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
		vEcritureComptable.setDate(new Date());
		vEcritureComptable.setLibelle("Libelle");
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(1), null, new BigDecimal(123), null));
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(2), null, null, new BigDecimal(1234)));
		manager.checkEcritureComptable(vEcritureComptable);
	}

	@Test(expected = FunctionalException.class)
	public void checkEcritureComptableRG3() throws Exception {
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
		vEcritureComptable.setDate(new Date());
		vEcritureComptable.setLibelle("Libelle");
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(1), null, null, null));
		vEcritureComptable.getListLigneEcriture().add(
				new LigneEcritureComptable(new CompteComptable(1), null, new BigDecimal(123), new BigDecimal(123)));
		manager.checkEcritureComptable(vEcritureComptable);
	}
    
	// vérifier que l'année dans la référence correspond bien à la date de l'écriture, idem pour le code journal...
	@Test
	public void addReferenceWhenSequenceExistRG5() {
		
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		vEcritureComptable.setJournal(manager.getJournalComptableByCode("AC"));
		Date pDate = new Date(1451602800000L);
		vEcritureComptable.setDate(pDate);
		vEcritureComptable.setLibelle("Test");
		CompteComptable compte401 = manager.getCompteComptableByCode(401);
		CompteComptable compte512 = manager.getCompteComptableByCode(512);
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(compte401, null, new BigDecimal(123), null));
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(compte512, null, null, new BigDecimal(123)));
		
		manager.addReference(vEcritureComptable);
		
		String reference = vEcritureComptable.getReference();
		char[] ca = {reference.charAt(3), reference.charAt(4), reference.charAt(5), reference.charAt(6)};
		String strYear = String.copyValueOf(ca);
		Calendar calendar = new GregorianCalendar();
    	calendar.setTime(vEcritureComptable.getDate());
    	//recuperation de l'année de l'ecriture
    	int yearFromTest = calendar.get(Calendar.YEAR);
    	int yearFromReference = Integer.parseInt(strYear);
    	
    	Assert.assertTrue(yearFromTest == yearFromReference);
    	getTransactionManager().rollbackMyERP(vTS);
				
	}
	@Test
	public void addReferenceWhenSequenceNullRG5() {
		
		EcritureComptable vEcritureComptable;
		vEcritureComptable = new EcritureComptable();
		vEcritureComptable.setJournal(manager.getJournalComptableByCode("BQ"));
		Date pDate = new Date();
		vEcritureComptable.setDate(pDate);
		vEcritureComptable.setLibelle("Test");
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(401), null, new BigDecimal(123), null));
		vEcritureComptable.getListLigneEcriture()
				.add(new LigneEcritureComptable(new CompteComptable(512), null, null, new BigDecimal(123)));
		
		manager.addReference(vEcritureComptable);
		
		String reference = vEcritureComptable.getReference();
		char[] ca = {reference.charAt(3), reference.charAt(4), reference.charAt(5), reference.charAt(6)};
		String strYear = String.copyValueOf(ca);
		Calendar calendar = new GregorianCalendar();
    	calendar.setTime(vEcritureComptable.getDate());
    	//recuperation de l'année de l'ecriture
    	int yearFromTest = calendar.get(Calendar.YEAR);
    	int yearFromReference = Integer.parseInt(strYear);
    	
    	Assert.assertTrue(yearFromTest == yearFromReference);
    	Assert.assertTrue("BQ-2019/00001".contentEquals(reference));
    	getTransactionManager().rollbackMyERP(vTS);
				
	}
	
	@Test
	public void updateEcritureComptable() {
		EcritureComptable vEcritureOld;
		EcritureComptable vEcritureComptable;
		EcritureComptable vEcritureNew = new EcritureComptable();
		vEcritureOld= manager.getEcritureComptable(-1);
		
		vEcritureNew.setId(-1);
		vEcritureNew.setJournal(manager.getJournalComptableByCode("OD"));
		Date pDate = new Date(1483138800000L);
		vEcritureNew.setDate(pDate);
		vEcritureNew.setLibelle("Cartouches d'imprimante");
		manager.addReference(vEcritureNew);
		
		
		vEcritureComptable= manager.getEcritureComptable(-1);
		Assert.assertTrue(vEcritureNew.getReference().contentEquals(vEcritureComptable.getReference()));
		Assert.assertFalse(vEcritureOld.getReference().contentEquals(vEcritureComptable.getReference()));
		
		getTransactionManager().rollbackMyERP(vTS);
		
	}

}
