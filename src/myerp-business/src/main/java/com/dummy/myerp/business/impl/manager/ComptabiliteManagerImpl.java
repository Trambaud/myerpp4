package com.dummy.myerp.business.impl.manager;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.TransactionStatus;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;


/**
 * Comptabilite manager implementation.
 */
public class ComptabiliteManagerImpl extends AbstractBusinessManager implements ComptabiliteManager {

    // ==================== Attributs ====================


    // ==================== Constructeurs ====================
    /**
     * Instantiates a new Comptabilite manager.
     */
    public ComptabiliteManagerImpl() {
    }


    // ==================== Getters/Setters ====================
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return getDaoProxy().getComptabiliteDao().getListCompteComptable();
    }


    @Override
    public List<JournalComptable> getListJournalComptable() {
        return getDaoProxy().getComptabiliteDao().getListJournalComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return getDaoProxy().getComptabiliteDao().getListEcritureComptable();
        
    }
    
    @Override
    public JournalComptable getJournalComptableByCode(String codeJrnl) {
    	
    	
    	List<JournalComptable> journalComptableList = getListJournalComptable();
    	JournalComptable journalComptable = new JournalComptable();
    	
    	for(JournalComptable pJrnl : journalComptableList) {
    		if (codeJrnl.contentEquals(pJrnl.getCode())) {
    			journalComptable = pJrnl;
    		}
    		
    	}
		return journalComptable ;
	}

    @Override
    public CompteComptable getCompteComptableByCode(int numeroCompte) {
    	List<CompteComptable> compteComptableList = getListCompteComptable();
    	CompteComptable compteComptable = new CompteComptable();
    	
    	for(CompteComptable pCompte : compteComptableList) {
    		if (numeroCompte == pCompte.getNumero()) {
    			compteComptable = pCompte;
    		}
    		
    	}
		return compteComptable ;
	}
    
    
	@Override
	public SequenceEcritureComptable getSequenceJournal(String codeJournal, Integer year) {
		try {
		return getDaoProxy().getComptabiliteDao().getSequenceEcritureComptableByCodeAndYear(codeJournal, year);
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}		
	}


	/**
     * {@inheritDoc}
     */
   
    @Override
    public synchronized void addReference(EcritureComptable pEcritureComptable) {
        
    	Calendar calendar = new GregorianCalendar();
    	calendar.setTime(pEcritureComptable.getDate());
    	//recuperation de l'année de l'ecriture
    	int year = calendar.get(Calendar.YEAR);
    	//recuperation du code Journal de l'ecriture
    	String codeJrnl=pEcritureComptable.getJournal().getCode();
    	
    	//1 Remonter depuis la persitance la dernière valeur de la séquence du journal pour l'année de l'écriture
        //(table sequence_ecriture_comptable)
    	
    	SequenceEcritureComptable sequenceEcritureComptable = getSequenceJournal(codeJrnl,year);
    	
    	if(sequenceEcritureComptable==null) {
    		//si return == null --> creation nouvelle ligne
    		sequenceEcritureComptable=new SequenceEcritureComptable();
    		sequenceEcritureComptable.setAnnee(year);
    		sequenceEcritureComptable.setDerniereValeur(1);
    		JournalComptable journalComptable = getJournalComptableByCode(codeJrnl);
    		sequenceEcritureComptable.setJournal(journalComptable);
    		// format sur 5 chiffres
        	DecimalFormat nbFormat = new DecimalFormat ("00000");
        	// creation string reference
        	String reference = codeJrnl + "-" + year + "/" + nbFormat.format(1);
        	//ajout à l'écriture comptable puis update sur la base
        	pEcritureComptable.setReference(reference);
    	}
    	else {
//    		 else incrementation +1 sur la valeur
    		int i = sequenceEcritureComptable.getDerniereValeur();
    		sequenceEcritureComptable.setDerniereValeur(i+1);
    		// format sur 5 chiffres
        	DecimalFormat nbFormat = new DecimalFormat ("00000");
        	// creation string reference
        	String reference = codeJrnl + "-" + year + "/" + nbFormat.format(sequenceEcritureComptable.getDerniereValeur());
        	//ajout à l'écriture comptable puis update sur la base
        	pEcritureComptable.setReference(reference);
    		
    	}
    	
    	try {
			insertEcritureComptable(pEcritureComptable);
		   	 
			
		} catch (FunctionalException e) {
				updateEcritureComptable(pEcritureComptable);
		}
    	
    	
    	
    	
    }

   

	/**
     * {@inheritDoc}
     */
    // TODO à tester
    @Override
    public void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptableUnit(pEcritureComptable);
        this.checkEcritureComptableContext(pEcritureComptable);
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion unitaires,
     * c'est à dire indépendemment du contexte (unicité de la référence, exercie comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    // TODO tests à compléter
    protected void checkEcritureComptableUnit(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== Vérification des contraintes unitaires sur les attributs de l'écriture
        Set<ConstraintViolation<EcritureComptable>> vViolations = getConstraintValidator().validate(pEcritureComptable);
        if (!vViolations.isEmpty()) {
            throw new FunctionalException("L'écriture comptable ne respecte pas les règles de gestion.",
                                          new ConstraintViolationException(
                                              "L'écriture comptable ne respecte pas les contraintes de validation",
                                              vViolations));
        }

        // ===== RG_Compta_2 : Pour qu'une écriture comptable soit valide, elle doit être équilibrée
        if (!pEcritureComptable.isEquilibree()) {
            throw new FunctionalException("L'écriture comptable n'est pas équilibrée.");
        }

        // ===== RG_Compta_3 : une écriture comptable doit avoir au moins 2 lignes d'écriture (1 au débit, 1 au crédit)
        int vNbrCredit = 0;
        int vNbrDebit = 0;
        for (LigneEcritureComptable vLigneEcritureComptable : pEcritureComptable.getListLigneEcriture()) {
        	int var = 0;
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getCredit(),
                                                                    BigDecimal.ZERO)) != 0 && var == 0) {
                vNbrCredit++;
                var++;
            }
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getDebit(),
                                                                    BigDecimal.ZERO)) != 0 && var == 0) {
                vNbrDebit++;
                var++;
            }
        }
        // On test le nombre de lignes car si l'écriture à une seule ligne
        //      avec un montant au débit et un montant au crédit ce n'est pas valable
        if (pEcritureComptable.getListLigneEcriture().size() < 2
            || vNbrCredit < 1
            || vNbrDebit < 1) {
            throw new FunctionalException(
                "L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");
        }

        // TODO ===== RG_Compta_5 : Format et contenu de la référence
        // vérifier que l'année dans la référence correspond bien à la date de l'écriture, idem pour le code journal...
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion liées au contexte
     * (unicité de la référence, année comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    protected void checkEcritureComptableContext(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== RG_Compta_6 : La référence d'une écriture comptable doit être unique
        if (StringUtils.isNoneEmpty(pEcritureComptable.getReference())) {
            try {
                // Recherche d'une écriture ayant la même référence
                EcritureComptable vECRef = getDaoProxy().getComptabiliteDao().getEcritureComptableByRef(pEcritureComptable.getReference());

                // Si l'écriture à vérifier est une nouvelle écriture (id == null),
                // ou si elle ne correspond pas à l'écriture trouvée (id != idECRef),
                // c'est qu'il y a déjà une autre écriture avec la même référence
                if (pEcritureComptable.getId() == null
                    || !pEcritureComptable.getId().equals(vECRef.getId())) {
                    throw new FunctionalException("Une autre écriture comptable existe déjà avec la même référence.");
                }
            } catch (NotFoundException vEx) {
                // Dans ce cas, c'est bon, ça veut dire qu'on n'a aucune autre écriture avec la même référence.
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().insertEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().deleteEcritureComptable(pId);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }


	@Override
	public EcritureComptable getEcritureComptable(int pId) {
		
		try {
			return getDaoProxy().getComptabiliteDao().getEcritureComptable(pId);
		} catch (NotFoundException e) {
			return null;
		}
		
	}



}
