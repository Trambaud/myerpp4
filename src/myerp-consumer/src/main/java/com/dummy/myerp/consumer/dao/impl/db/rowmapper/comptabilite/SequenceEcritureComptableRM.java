package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dummy.myerp.consumer.dao.impl.cache.JournalComptableDaoCache;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;

public class SequenceEcritureComptableRM  implements RowMapper<SequenceEcritureComptable> {

	 /** JournalComptableDaoCache */
    private final JournalComptableDaoCache journalComptableDaoCache = new JournalComptableDaoCache();
    
	@Override
	public SequenceEcritureComptable mapRow(ResultSet rs, int rowNum) throws SQLException {
		SequenceEcritureComptable vBean = new SequenceEcritureComptable();
        vBean.setAnnee(rs.getInt("annee"));
        vBean.setDerniereValeur(rs.getInt("derniere_valeur"));
        vBean.setJournal(journalComptableDaoCache.getByCode(rs.getString("journal_code")));
        return vBean;
	}

}
