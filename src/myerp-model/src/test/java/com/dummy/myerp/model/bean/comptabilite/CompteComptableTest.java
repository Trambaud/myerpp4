package com.dummy.myerp.model.bean.comptabilite;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CompteComptableTest {

	@Test
	public void getByNumeroGranted() {
		CompteComptable compteComptable = new CompteComptable();
		compteComptable.setNumero(1);
		compteComptable.setLibelle("Compte de Test");
		CompteComptable compteComptable2 = new CompteComptable(2, "Compte de Test 2");	
		List<CompteComptable> testList = new ArrayList<CompteComptable>();
		testList.add(compteComptable);
		testList.add(compteComptable2);
		Assert.assertEquals(compteComptable2 ,CompteComptable.getByNumero(testList, 2));
		
						
		
	}
	
	@Test
	public void getByNumeroWhenSecondConditionFalse() {
		CompteComptable compteComptable = new CompteComptable();
		compteComptable.setNumero(1);
		compteComptable.setLibelle("Compte de Test");
		List<CompteComptable> testList = new ArrayList<CompteComptable>();
		testList.add(compteComptable);
		Assert.assertNotEquals(compteComptable, CompteComptable.getByNumero(testList, 3));
		
						
		
	}
	@Test
	public void getByNumeroWhenFirstConditionFalse() {
		CompteComptable compteComptable = null;
			
		List<CompteComptable> testList = new ArrayList<CompteComptable>();
		testList.add(compteComptable);
		
		Assert.assertEquals(compteComptable, CompteComptable.getByNumero(testList, 2));
		
						
		
	}
}
