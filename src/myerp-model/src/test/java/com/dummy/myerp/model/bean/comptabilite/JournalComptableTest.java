package com.dummy.myerp.model.bean.comptabilite;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class JournalComptableTest {

	@Test
	public void getByCodeGranted() {
		JournalComptable journalComptable = new JournalComptable();
		journalComptable.setCode("1");
		journalComptable.setLibelle("Journal de Test");
		JournalComptable journalComptable2 = new JournalComptable("2", "Journal de Test 2");	
		List<JournalComptable> testList = new ArrayList<JournalComptable>();
		testList.add(journalComptable);
		testList.add(journalComptable2);
		
		
		JournalComptable resultat = JournalComptable.getByCode(testList, "2");
		
		
		Assert.assertEquals(journalComptable2 ,resultat);
		
						
		
	}
	
	@Test
	public void getByCodeWhenSecondConditionFalse() {
		JournalComptable journalComptable = new JournalComptable();
		journalComptable.setCode("1");
		journalComptable.setLibelle("Journal de Test");
		List<JournalComptable> testList = new ArrayList<JournalComptable>();
		testList.add(journalComptable);
		
		
		Assert.assertNotEquals(journalComptable, JournalComptable.getByCode(testList, "3"));
		
						
		
	}
	@Test
	public void getByCodeWhenFirstConditionFalse() {
		JournalComptable journalComptable = null;
			
		List<JournalComptable> testList = new ArrayList<JournalComptable>();
		testList.add(journalComptable);
		
		Assert.assertEquals(journalComptable, JournalComptable.getByCode(testList, "2"));
		
						
		
	}

}
